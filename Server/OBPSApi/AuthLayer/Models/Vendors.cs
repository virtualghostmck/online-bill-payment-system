﻿using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public partial class Vendors
    {
        public Vendors()
        {
            CustomerVendors = new HashSet<CustomerVendors>();
            VendorCategories = new HashSet<VendorCategories>();
            VendorsCities = new HashSet<VendorsCities>();
        }

        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string VendorAddress { get; set; }
        public string ContactInfoContactNo { get; set; }
        public string ContactInfoEmail { get; set; }
        public string Gstin { get; set; }

        public ICollection<CustomerVendors> CustomerVendors { get; set; }
        public ICollection<VendorCategories> VendorCategories { get; set; }
        public ICollection<VendorsCities> VendorsCities { get; set; }
    }
}
