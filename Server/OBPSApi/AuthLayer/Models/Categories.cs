﻿using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public partial class Categories
    {
        public Categories()
        {
            CustomerVendors = new HashSet<CustomerVendors>();
            VendorCategories = new HashSet<VendorCategories>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }

        public ICollection<CustomerVendors> CustomerVendors { get; set; }
        public ICollection<VendorCategories> VendorCategories { get; set; }
    }
}
