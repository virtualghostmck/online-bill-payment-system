﻿using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public partial class VendorCategories
    {
        public int CategoryId { get; set; }
        public int VendorId { get; set; }
        public int Amount { get; set; }

        public Categories Category { get; set; }
        public Vendors Vendor { get; set; }
    }
}
