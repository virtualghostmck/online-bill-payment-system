﻿using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public partial class CustomerVendors
    {
        public CustomerVendors()
        {
            Transactions = new HashSet<Transactions>();
        }

        public string UniqueId { get; set; }
        public int AmountDue { get; set; }
        public int? CustomerId { get; set; }
        public int VendorId { get; set; }
        public int CategoryId { get; set; }

        public Categories Category { get; set; }
        public Customers Customer { get; set; }
        public Vendors Vendor { get; set; }
        public ICollection<Transactions> Transactions { get; set; }
    }
}
