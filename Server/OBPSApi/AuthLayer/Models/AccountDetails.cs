﻿using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public partial class AccountDetails
    {
        public int CustomerId { get; set; }
        public long BankAcNo { get; set; }
        public string BankName { get; set; }
        public long Ifsc { get; set; }
        public string BranchName { get; set; }
        public long Micrcode { get; set; }

        public Customers Customer { get; set; }
    }
}
