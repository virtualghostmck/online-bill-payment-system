﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AuthLayer.Models
{
    public partial class BillPaymentContext : IdentityDbContext<Customers,IdentityRole<int>,int,IdentityUserClaim<int>, IdentityUserRole<int>,IdentityUserLogin<int>,IdentityRoleClaim<int>,IdentityUserToken<int>>
    {
        public BillPaymentContext()
        {
        }
        public BillPaymentContext(DbContextOptions<BillPaymentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountDetails> AccountDetails { get; set; }
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<Cities> Cities { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<CustomerVendors> CustomerVendors { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<VendorCategories> VendorCategories { get; set; }
        public virtual DbSet<Vendors> Vendors { get; set; }
        public virtual DbSet<VendorsCities> VendorsCities { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=.;Initial Catalog=BillPayment_new;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AccountDetails>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.HasIndex(e => e.BankAcNo)
                    .HasName("IX_BankAcNo")
                    .IsUnique();

                entity.HasIndex(e => e.CustomerId)
                    .HasName("IX_CustomerID");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BankName).IsRequired();

                entity.Property(e => e.BranchName).IsRequired();

                entity.Property(e => e.Ifsc).HasColumnName("IFSC");

                entity.Property(e => e.Micrcode).HasColumnName("MICRCode");

                entity.HasOne(d => d.Customer)
                    .WithOne(p => p.AccountDetails)
                    .HasForeignKey<AccountDetails>(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.AccountDetails_dbo.Customers_CustomerID");
            });

            modelBuilder.Entity<Categories>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.CategoryName).IsRequired();

                entity.Property(e => e.Description).HasMaxLength(50);
            });

            modelBuilder.Entity<Cities>(entity =>
            {
                entity.HasKey(e => e.CityId);

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CityName).IsRequired();
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.Ignore(prop => prop.AccessFailedCount);
                entity.Ignore(prop => prop.ConcurrencyStamp);
                entity.Ignore(prop => prop.EmailConfirmed);
                entity.Ignore(prop => prop.PhoneNumberConfirmed);
                entity.Ignore(prop => prop.TwoFactorEnabled);
                entity.Ignore(prop => prop.SecurityStamp);
                entity.Ignore(prop => prop.LockoutEnabled);
                entity.Ignore(prop => prop.Id);
                entity.Ignore(prop => prop.LockoutEnd);

                entity.HasKey(e => e.CustomerId);

                entity.HasIndex(e => e.CityId)
                    .HasName("IX_CityID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.ContactInfoContactNo)
                    .IsRequired()
                    .HasColumnName("ContactInfo_ContactNo")
                    .HasMaxLength(10);

                entity.Property(e => e.ContactInfoEmail).HasColumnName("ContactInfo_Email");

                entity.Property(e => e.CustomerAddress).HasMaxLength(50);

                entity.Property(e => e.CustomerName).IsRequired();

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_dbo.Customers_dbo.Cities_CityID");
            });
            modelBuilder.Entity<Customers>().ToTable("Customers");

            modelBuilder.Entity<CustomerVendors>(entity =>
            {
                entity.HasKey(e => e.UniqueId);

                entity.HasIndex(e => e.CategoryId)
                    .HasName("IX_CategoryID");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("IX_CustomerID");

                entity.HasIndex(e => e.VendorId)
                    .HasName("IX_VendorID");

                entity.Property(e => e.UniqueId)
                    .HasColumnName("UniqueID")
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.CustomerVendors)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_dbo.CustomerVendors_dbo.Categories_CategoryID");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerVendors)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_dbo.CustomerVendors_dbo.Customers_CustomerID");

                entity.HasOne(d => d.Vendor)
                    .WithMany(p => p.CustomerVendors)
                    .HasForeignKey(d => d.VendorId)
                    .HasConstraintName("FK_dbo.CustomerVendors_dbo.Vendors_VendorID");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey });

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Transactions>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.HasIndex(e => e.UniqueId)
                    .HasName("IX_UniqueID");

                entity.Property(e => e.TransactionId)
                    .HasColumnName("TransactionID")
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.TransactionDate).HasColumnType("datetime");

                entity.Property(e => e.UniqueId)
                    .HasColumnName("UniqueID")
                    .HasMaxLength(128);

                entity.HasOne(d => d.Unique)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.UniqueId)
                    .HasConstraintName("FK_dbo.Transactions_dbo.CustomerVendors_UniqueID");
            });

            modelBuilder.Entity<VendorCategories>(entity =>
            {
                entity.HasKey(e => new { e.CategoryId, e.VendorId });

                entity.HasIndex(e => e.CategoryId)
                    .HasName("IX_CategoryID");

                entity.HasIndex(e => e.VendorId)
                    .HasName("IX_VendorID");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.VendorCategories)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_dbo.VendorCategories_dbo.Categories_CategoryID");

                entity.HasOne(d => d.Vendor)
                    .WithMany(p => p.VendorCategories)
                    .HasForeignKey(d => d.VendorId)
                    .HasConstraintName("FK_dbo.VendorCategories_dbo.Vendors_VendorID");
            });

            modelBuilder.Entity<Vendors>(entity =>
            {
                entity.HasKey(e => e.VendorId);

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.ContactInfoContactNo)
                    .IsRequired()
                    .HasColumnName("ContactInfo_ContactNo")
                    .HasMaxLength(10);

                entity.Property(e => e.ContactInfoEmail).HasColumnName("ContactInfo_Email");

                entity.Property(e => e.Gstin)
                    .HasColumnName("GSTIN")
                    .HasMaxLength(15);

                entity.Property(e => e.VendorAddress).HasMaxLength(50);

                entity.Property(e => e.VendorName).IsRequired();
            });

            modelBuilder.Entity<VendorsCities>(entity =>
            {
                entity.HasKey(e => new { e.VendorsVendorId, e.CityCityId });

                entity.HasIndex(e => e.CityCityId)
                    .HasName("IX_City_CityID");

                entity.HasIndex(e => e.VendorsVendorId)
                    .HasName("IX_Vendors_VendorID");

                entity.Property(e => e.VendorsVendorId).HasColumnName("Vendors_VendorID");

                entity.Property(e => e.CityCityId).HasColumnName("City_CityID");

                entity.HasOne(d => d.CityCity)
                    .WithMany(p => p.VendorsCities)
                    .HasForeignKey(d => d.CityCityId)
                    .HasConstraintName("FK_dbo.VendorsCities_dbo.Cities_City_CityID");

                entity.HasOne(d => d.VendorsVendor)
                    .WithMany(p => p.VendorsCities)
                    .HasForeignKey(d => d.VendorsVendorId)
                    .HasConstraintName("FK_dbo.VendorsCities_dbo.Vendors_Vendors_VendorID");
            });
        }
    }
}
