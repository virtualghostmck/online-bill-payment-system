﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public class Customers : IdentityUser<int>
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Gender { get; set; }
        public string CustomerAddress { get; set; }
        public string ContactInfoContactNo { get; set; }
        public string ContactInfoEmail { get; set; }
        public string Password { get; set; }
        public int CityId { get; set; }

        public Cities City { get; set; }
        public AccountDetails AccountDetails { get; set; }
        public ICollection<CustomerVendors> CustomerVendors { get; set; } = new HashSet<CustomerVendors>();
    }
}
