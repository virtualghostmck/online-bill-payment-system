﻿using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public partial class Cities
    {
        public Cities()
        {
            Customers = new HashSet<Customers>();
            VendorsCities = new HashSet<VendorsCities>();
        }

        public int CityId { get; set; }
        public string CityName { get; set; }

        public ICollection<Customers> Customers { get; set; }
        public ICollection<VendorsCities> VendorsCities { get; set; }
    }
}
