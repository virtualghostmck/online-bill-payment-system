﻿using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public partial class Transactions
    {
        public string TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
        public int Amount { get; set; }
        public string UniqueId { get; set; }

        public CustomerVendors Unique { get; set; }
    }
}
