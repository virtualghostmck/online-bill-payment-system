﻿using System;
using System.Collections.Generic;

namespace AuthLayer.Models
{
    public partial class VendorsCities
    {
        public int VendorsVendorId { get; set; }
        public int CityCityId { get; set; }

        public Cities CityCity { get; set; }
        public Vendors VendorsVendor { get; set; }
    }
}
