﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthLayer.Models;

namespace AuthLayer.BLLServices
{
    public class CategoryService : ICategoryService
    {
        private readonly BillPaymentContext _context;
        public CategoryService(BillPaymentContext context)
        {
            _context = context;
        }
        public List<Categories> GetCategories()
        {
            var categories = _context.Categories.ToList();
            return categories;
        }

        public List<Vendors> GetVendorsInCategory(string categoryName)
        {
            var categoryID = GetCategoryIDByName(categoryName);
            var vendors = _context.Vendors.Where(v => v.VendorCategories.Where(vc => vc.CategoryId == categoryID).Select(vc => vc.VendorId).Contains(v.VendorId)).Select(v=>v);
            return vendors.ToList();
        }

        public int GetCategoryIDByName(string name)
        {
            var categories = _context.Categories.SingleOrDefault(c => c.CategoryName.ToLower() == name.ToLower());
            return categories.CategoryId;
        }

    }
}
