﻿using AuthLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthLayer.BLLServices
{
    public interface ICategoryService
    {
        List<Categories> GetCategories();
        List<Vendors> GetVendorsInCategory(string categoryName);
        int GetCategoryIDByName(string name);
      
    }
}
