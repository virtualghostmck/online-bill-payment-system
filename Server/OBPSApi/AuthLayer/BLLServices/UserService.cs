﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AuthLayer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace AuthLayer.BLLServices
{
    public class UserService : IUserService
    {
        private readonly UserManager<Customers> _userManager;
        private readonly BillPaymentContext _context;
        public UserService(UserManager<Customers> userManager, BillPaymentContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        public async Task<IdentityResult> CreateUser(Customers user, string password)
        {
            var userFound = await _userManager.FindByEmailAsync(user.Email.ToUpper());
            if (userFound == null)
            {
                var cityID = GetCityID(user.CustomerAddress);
                if (cityID < 0)
                {
                    return IdentityResult.Failed(new IdentityError[] { new IdentityError { Code = "445", Description = "Service not available in your city" } });
                }
                else
                {
                    var result = await _userManager.CreateAsync(user, password);
                    return result;
                }
            }
            else
            {
                return IdentityResult.Failed(new IdentityError[] { new IdentityError { Code = "444", Description = "Invalid Email"} });
            }
            
        }

        public async Task<Customers> FetchUser(Customers user, string password)
        {
            Customers userFound = await _userManager.FindByEmailAsync(user.Email);
            if (await _userManager.CheckPasswordAsync(userFound, password))
            {
                return userFound;
            }
            else
            {
                return null;
            }
        }

        public async Task<Customers> GetProfile(Customers user)
        {
            Customers userFound = await _userManager.FindByEmailAsync(user.Email);
            return userFound;
        }

        public int GetCityID(string cityName)
        {
           var city = _context.Cities.SingleOrDefault(c => c.CityName.ToLower() == cityName.ToLower());
            if (city != null)
            {
                return city.CityId;
            }
            else
            {
                return -1;
            }
        }
    }
}
