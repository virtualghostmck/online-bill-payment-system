﻿using AuthLayer.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AuthLayer.BLLServices
{
    public interface IUserService
    {
        Task<IdentityResult> CreateUser(Customers user, string password);
        Task<Customers> FetchUser(Customers user, string password);

        Task<Customers> GetProfile(Customers user);

        int GetCityID(string cityName);
    }
}
