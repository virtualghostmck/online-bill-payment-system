﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using AuthLayer.BLLServices;
using AuthLayer.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using OBPSApi.Model;
using System.Security.Claims;
using System.Text;
using OBPSApi.Helpers;
using Microsoft.Extensions.Options;

namespace OBPSApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IUserService _userService;
        private readonly AppSettings _appSettings;
        public AccountController(IUserService userService, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _appSettings = appSettings.Value;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                Customers user = new Customers() {
                    CustomerName = userModel.UserName,
                    UserName = userModel.UserName,
                    Password = userModel.Password,
                    ContactInfoContactNo = userModel.ContactNo,
                    Email = userModel.Email,
                    NormalizedEmail = userModel.Email,
                    NormalizedUserName = userModel.UserName,
                    CustomerAddress = userModel.CityName
                };

                var result = await _userService.CreateUser(user, user.Password);

                if (!result.Succeeded)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(error.Code, error.Description);
                    }
                    return BadRequest(ModelState);
                }

                return Created("Login", user);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);   
            }
           
        }

        [HttpPost("login",Name ="Login")]
        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = await _userService.FetchUser(new Customers { Email = loginModel.Email, Password = loginModel.Password }, loginModel.Password);

                if (user == null)
                {
                    return BadRequest(new { message = "Username or password is incorrect" });
                }
                else
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                    new Claim(ClaimTypes.Name, user.CustomerId.ToString())
                        }),
                        Expires = DateTime.UtcNow.AddDays(1),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    loginModel.Token = tokenHandler.WriteToken(token);
                    loginModel.UserName = user.UserName;

                    // remove password before returning
                    loginModel.Password = null;
                    return Ok(loginModel);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpGet("profile/{email}", Name ="Profile")]
        public async Task<IActionResult> GetProfile(string email)
        {
            try
            {
                Customers customer = new Customers { Email = email };
                var profile = await _userService.GetProfile(customer);
                if (profile != null)
                {
                    profile.Password = null;
                    profile.PasswordHash = null;
                    return Ok(profile);
                }
                else
                {
                    return BadRequest("Profile not found");
                }
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}