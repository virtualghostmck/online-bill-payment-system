﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthLayer.BLLServices;
using AuthLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OBPSApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        // GET: api/Category
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var categories = _categoryService.GetCategories();
                if (categories.Count == 0)
                {
                    return BadRequest(new { message = "No categories have been configured", code = "404" });
                }
                else
                {
                    return Ok(categories);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // GET: api/Category/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Category
        //[HttpPost(Name ="AddCategory")]
        //public void AddCategory([FromBody] string value)
        //{
        //}

        //// PUT: api/Category/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
