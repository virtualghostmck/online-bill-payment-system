﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthLayer.BLLServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OBPSApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class VendorController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        public VendorController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [AllowAnonymous]
        [HttpGet("{categoryName}")]
        public IActionResult GetVendorInCategory(string categoryName)
        {
            try
            {
                var vendors = _categoryService.GetVendorsInCategory(categoryName);
                return Ok(vendors);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}