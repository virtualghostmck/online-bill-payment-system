﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OBPSApi.Model
{
    public class UserModel
    {
        [Required]
        [DataType(DataType.Text)]
        public string UserName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string ContactNo { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string CityName { get; set; }
    }
}
