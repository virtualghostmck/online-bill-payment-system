USE [BillPayment_new]
GO
SET IDENTITY_INSERT [dbo].[Cities] ON 

INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (1, N'Mumbai')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (2, N'Pune')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (3, N'Delhi')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (4, N'Kolkata')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (5, N'Nagpur')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (6, N'Chennai')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (7, N'Hyderabad')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (8, N'Indore')
SET IDENTITY_INSERT [dbo].[Cities] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', NULL, N'AQAAAAEAACcQAAAAENBnq8fQYAijdCYnADetbsNnH/sJaHuB2X8s+8t1lc6eikVTBPsTWGKt9IQEPTatTA==', NULL, 5, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEHLzHqe8uIkFTvZ1NxY82b+VQJaWupojopkkF606MsccHx6gyL59KVyHEP96LXQ65w==', NULL, 6, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEKIZWTGI6z3VpC+kNV/1CA5YgwylKVC47+Bi0SqmAWWNG7hBi8BdyM3v997oXZF//g==', NULL, 7, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEFGlYcztYlLl3eUaAMMsKht9v7+++dynF2TepEKf6tMwRSz/3iztj15thynw0HhJ6A==', NULL, 8, N'test', NULL, NULL, N'9422255544', N'test@test.com', N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEFTUuuVQYGk3k1xgDaAhFes2NHRYLQDN5f1OVwwZ4Z/lJnFolM3qqWq8+jZ+K/i7wg==', NULL, 9, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEK7vXzhzrQ2yFqojtZJahw6LwzelbLBm4/ZUsAtZTOm/LuWvj+0n3ZW7dfjrJRroJA==', NULL, 10, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, N'TEST@TEST.COM', N'TEST')
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (1, N'Mobile', N'Mobile Recharge')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (2, N'Landline', N'Pay Landline Bill')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (3, N'DTH', N'Recharge your plan')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (4, N'Electricity', N'Pay Electricity Bill')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (5, N'Gas', N'Pay Gas Bill')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Vendors] ON 

INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (1, N'JIO', N'Mumbai', N'8956871235', N'jio@jio.com', N'3234322EN43')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (2, N'IDEA', N'Delhi', N'6589784258', N'idea@idea.com', N'8978564GC55')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (3, N'MSEDCL', N'Pune', N'8955224466', N'msedcl@msedcl.com', N'78544554RC7')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (4, N'HP', N'Pune', N'8845452258', N'hp@hp.com', N'885454GDFDF')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (5, N'Reliance Power', N'Mumbai', N'7855455588', N'reliance@123.com', N'458787DSD44')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (6, N'Indane', N'Nagpur', N'9898846456', N'indane@indane.com', N'5648FDF4654')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (7, N'BSNL', N'Delhi', N'7884554524', N'bsnl@bsnl.com', N'56456RRTG54')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (8, N'Docomo ', N'Delhi', N'8745441122', N'docomo@dcmo.com', N'5787968EDDF')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (9, N'Tata Sky', N'Mumbai', N'8844552255', N'tatasky@sky.com', N'874FD4564DD')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (10, N'Videocon', N'Pune', N'8798798754', N'video@video.com', N'564DSDS4564')
SET IDENTITY_INSERT [dbo].[Vendors] OFF
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (1, 1, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (1, 2, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (1, 7, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (2, 7, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (2, 8, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (3, 9, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (3, 10, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (4, 3, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (4, 5, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (5, 4, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (5, 6, 200)
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200228115653_authModel', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200306092101_customerUpdated', N'2.1.14-servicing-32113')
