USE [BillPayment_new]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountDetails]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDetails](
	[CustomerID] [int] NOT NULL,
	[BankAcNo] [bigint] NOT NULL,
	[BankName] [nvarchar](max) NOT NULL,
	[IFSC] [bigint] NOT NULL,
	[BranchName] [nvarchar](max) NOT NULL,
	[MICRCode] [bigint] NOT NULL,
 CONSTRAINT [PK_AccountDetails] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [int] NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cities](
	[CityID] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[UserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [nvarchar](max) NOT NULL,
	[Gender] [nvarchar](max) NULL,
	[CustomerAddress] [nvarchar](50) NULL,
	[ContactInfo_ContactNo] [nvarchar](10) NOT NULL,
	[ContactInfo_Email] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[CityID] [int] NOT NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerVendors]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerVendors](
	[UniqueID] [nvarchar](128) NOT NULL,
	[AmountDue] [int] NOT NULL,
	[CustomerID] [int] NULL,
	[VendorID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_CustomerVendors] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transactions](
	[TransactionID] [nvarchar](128) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Amount] [int] NOT NULL,
	[UniqueID] [nvarchar](128) NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VendorCategories]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VendorCategories](
	[CategoryID] [int] NOT NULL,
	[VendorID] [int] NOT NULL,
	[Amount] [int] NOT NULL,
 CONSTRAINT [PK_VendorCategories] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC,
	[VendorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vendors]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vendors](
	[VendorID] [int] IDENTITY(1,1) NOT NULL,
	[VendorName] [nvarchar](max) NOT NULL,
	[VendorAddress] [nvarchar](50) NULL,
	[ContactInfo_ContactNo] [nvarchar](10) NOT NULL,
	[ContactInfo_Email] [nvarchar](max) NULL,
	[GSTIN] [nvarchar](15) NULL,
 CONSTRAINT [PK_Vendors] PRIMARY KEY CLUSTERED 
(
	[VendorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VendorsCities]    Script Date: 3/19/2020 9:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VendorsCities](
	[Vendors_VendorID] [int] NOT NULL,
	[City_CityID] [int] NOT NULL,
 CONSTRAINT [PK_VendorsCities] PRIMARY KEY CLUSTERED 
(
	[Vendors_VendorID] ASC,
	[City_CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200228115653_authModel', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200306092101_customerUpdated', N'2.1.14-servicing-32113')
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (1, N'Mobile', N'Mobile Recharge')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (2, N'Landline', N'Pay Landline Bill')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (3, N'DTH', N'Recharge your plan')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (4, N'Electricity', N'Pay Electricity Bill')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [Description]) VALUES (5, N'Gas', N'Pay Gas Bill')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Cities] ON 

INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (1, N'Mumbai')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (2, N'Pune')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (3, N'Delhi')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (4, N'Kolkata')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (5, N'Nagpur')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (6, N'Chennai')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (7, N'Hyderabad')
INSERT [dbo].[Cities] ([CityID], [CityName]) VALUES (8, N'Indore')
SET IDENTITY_INSERT [dbo].[Cities] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', NULL, N'AQAAAAEAACcQAAAAENBnq8fQYAijdCYnADetbsNnH/sJaHuB2X8s+8t1lc6eikVTBPsTWGKt9IQEPTatTA==', NULL, 5, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEHLzHqe8uIkFTvZ1NxY82b+VQJaWupojopkkF606MsccHx6gyL59KVyHEP96LXQ65w==', NULL, 6, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEKIZWTGI6z3VpC+kNV/1CA5YgwylKVC47+Bi0SqmAWWNG7hBi8BdyM3v997oXZF//g==', NULL, 7, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEFGlYcztYlLl3eUaAMMsKht9v7+++dynF2TepEKf6tMwRSz/3iztj15thynw0HhJ6A==', NULL, 8, N'test', NULL, NULL, N'9422255544', N'test@test.com', N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEFTUuuVQYGk3k1xgDaAhFes2NHRYLQDN5f1OVwwZ4Z/lJnFolM3qqWq8+jZ+K/i7wg==', NULL, 9, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, NULL, NULL)
INSERT [dbo].[Customers] ([UserName], [Email], [PasswordHash], [PhoneNumber], [CustomerID], [CustomerName], [Gender], [CustomerAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [Password], [CityID], [NormalizedEmail], [NormalizedUserName]) VALUES (N'test', N'test@test.com', N'AQAAAAEAACcQAAAAEK7vXzhzrQ2yFqojtZJahw6LwzelbLBm4/ZUsAtZTOm/LuWvj+0n3ZW7dfjrJRroJA==', NULL, 10, N'test', NULL, NULL, N'9422255544', NULL, N'Test@123', 1, N'TEST@TEST.COM', N'TEST')
SET IDENTITY_INSERT [dbo].[Customers] OFF
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (1, 1, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (1, 2, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (1, 7, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (2, 7, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (2, 8, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (3, 9, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (3, 10, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (4, 3, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (4, 5, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (5, 4, 200)
INSERT [dbo].[VendorCategories] ([CategoryID], [VendorID], [Amount]) VALUES (5, 6, 200)
SET IDENTITY_INSERT [dbo].[Vendors] ON 

INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (1, N'JIO', N'Mumbai', N'8956871235', N'jio@jio.com', N'3234322EN43')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (2, N'IDEA', N'Delhi', N'6589784258', N'idea@idea.com', N'8978564GC55')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (3, N'MSEDCL', N'Pune', N'8955224466', N'msedcl@msedcl.com', N'78544554RC7')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (4, N'HP', N'Pune', N'8845452258', N'hp@hp.com', N'885454GDFDF')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (5, N'Reliance Power', N'Mumbai', N'7855455588', N'reliance@123.com', N'458787DSD44')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (6, N'Indane', N'Nagpur', N'9898846456', N'indane@indane.com', N'5648FDF4654')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (7, N'BSNL', N'Delhi', N'7884554524', N'bsnl@bsnl.com', N'56456RRTG54')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (8, N'Docomo ', N'Delhi', N'8745441122', N'docomo@dcmo.com', N'5787968EDDF')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (9, N'Tata Sky', N'Mumbai', N'8844552255', N'tatasky@sky.com', N'874FD4564DD')
INSERT [dbo].[Vendors] ([VendorID], [VendorName], [VendorAddress], [ContactInfo_ContactNo], [ContactInfo_Email], [GSTIN]) VALUES (10, N'Videocon', N'Pune', N'8798798754', N'video@video.com', N'564DSDS4564')
SET IDENTITY_INSERT [dbo].[Vendors] OFF
ALTER TABLE [dbo].[AccountDetails]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AccountDetails_dbo.Customers_CustomerID] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[AccountDetails] CHECK CONSTRAINT [FK_dbo.AccountDetails_dbo.Customers_CustomerID]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_Customers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Customers] ([CustomerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_Customers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_Customers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Customers] ([CustomerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_Customers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_Customers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Customers] ([CustomerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_Customers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_Customers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Customers] ([CustomerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_Customers_UserId]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Customers_dbo.Cities_CityID] FOREIGN KEY([CityID])
REFERENCES [dbo].[Cities] ([CityID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_dbo.Customers_dbo.Cities_CityID]
GO
ALTER TABLE [dbo].[CustomerVendors]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CustomerVendors_dbo.Categories_CategoryID] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([CategoryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerVendors] CHECK CONSTRAINT [FK_dbo.CustomerVendors_dbo.Categories_CategoryID]
GO
ALTER TABLE [dbo].[CustomerVendors]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CustomerVendors_dbo.Customers_CustomerID] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[CustomerVendors] CHECK CONSTRAINT [FK_dbo.CustomerVendors_dbo.Customers_CustomerID]
GO
ALTER TABLE [dbo].[CustomerVendors]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CustomerVendors_dbo.Vendors_VendorID] FOREIGN KEY([VendorID])
REFERENCES [dbo].[Vendors] ([VendorID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerVendors] CHECK CONSTRAINT [FK_dbo.CustomerVendors_dbo.Vendors_VendorID]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Transactions_dbo.CustomerVendors_UniqueID] FOREIGN KEY([UniqueID])
REFERENCES [dbo].[CustomerVendors] ([UniqueID])
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [FK_dbo.Transactions_dbo.CustomerVendors_UniqueID]
GO
ALTER TABLE [dbo].[VendorCategories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.VendorCategories_dbo.Categories_CategoryID] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([CategoryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VendorCategories] CHECK CONSTRAINT [FK_dbo.VendorCategories_dbo.Categories_CategoryID]
GO
ALTER TABLE [dbo].[VendorCategories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.VendorCategories_dbo.Vendors_VendorID] FOREIGN KEY([VendorID])
REFERENCES [dbo].[Vendors] ([VendorID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VendorCategories] CHECK CONSTRAINT [FK_dbo.VendorCategories_dbo.Vendors_VendorID]
GO
ALTER TABLE [dbo].[VendorsCities]  WITH CHECK ADD  CONSTRAINT [FK_dbo.VendorsCities_dbo.Cities_City_CityID] FOREIGN KEY([City_CityID])
REFERENCES [dbo].[Cities] ([CityID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VendorsCities] CHECK CONSTRAINT [FK_dbo.VendorsCities_dbo.Cities_City_CityID]
GO
ALTER TABLE [dbo].[VendorsCities]  WITH CHECK ADD  CONSTRAINT [FK_dbo.VendorsCities_dbo.Vendors_Vendors_VendorID] FOREIGN KEY([Vendors_VendorID])
REFERENCES [dbo].[Vendors] ([VendorID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[VendorsCities] CHECK CONSTRAINT [FK_dbo.VendorsCities_dbo.Vendors_Vendors_VendorID]
GO
