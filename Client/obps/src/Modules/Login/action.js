import { actionType as type } from "./type";

export const setUserData = currentUser => {
	return {
		type: type.SET_USER_DATA,
		payload: currentUser
	};
};

export const clearUserData = currentUser => {
	return {
		type: type.CLEAR_USER_DATA,
		payload: currentUser
	};
};

export const toggleLoginDialogue = () => {
	return {
		type: type.TOGGLE_LOGIN_DIALOG
	};
};
