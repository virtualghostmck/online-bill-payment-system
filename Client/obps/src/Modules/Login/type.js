import keyMirror from "keymirror";

export const actionType = keyMirror({
	SET_USER_DATA: null,
	CLEAR_USER_DATA: null,
	TOGGLE_LOGIN_DIALOG: null,
	TOGGLE_REGISTER_DIALOG: null
});
