import { actionType as type } from "./type";

const initState = {
	currentUser: null,
	isLoading: false,
	openLoginDialog: false
};

const userReducer = (state = initState, action) => {
	switch (action.type) {
		case type.SET_USER_DATA:
			return {
				...state,
				currentUser: action.payload,
				isLoading: false
			};
		case type.CLEAR_USER_DATA:
			return {
				...state,
				currentUser: null,
				isLoading: false
			};
		case type.TOGGLE_LOGIN_DIALOG:
			return {
				...state,
				openLoginDialog: !state.openLoginDialog
			};
		default:
			return state;
	}
};

export default userReducer;
