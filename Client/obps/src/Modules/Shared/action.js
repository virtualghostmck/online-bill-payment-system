import { ActionTypes as types } from "./type";

export const changeProfileData = profileData => {
  return {
    type: types.CHANGE_PROFILE_DATA,
    data: { profileData: profileData }
  };
};

export const toggleIsLoading = () => {
  return {
    type: types.TOGGLE_LOADING
  };
};
