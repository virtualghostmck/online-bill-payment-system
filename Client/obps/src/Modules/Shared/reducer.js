import { ActionTypes as actionType } from "./type";

var initialState = {
  siteName: "BILL 'O' PAY",
  footerLinks: [
    {
      link: "Some Link"
    },
    {
      link: "Some Link"
    },
    {
      link: "Some Link"
    }
  ],
  profileData: {
    name: "XYZ"
  },
  isLoggedIn: false,
  isLoading: true
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actionType.CHANGE_PROFILE_DATA:
      return {
        ...state,
        profileData: action.profileData
      };
    case actionType.TOGGLE_LOADING:
      return {
        ...state,
        isLoading: !state.isLoading
      };
    default:
      return state;
  }
}

export default reducer;
