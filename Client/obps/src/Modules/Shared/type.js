import keyMirror from "keymirror";

export const ActionTypes = keyMirror({
  // UI CHANGES
  CHANGE_PROFILE_DATA: null,
  TOGGLE_LOADING: null
  //API CALLS
});
