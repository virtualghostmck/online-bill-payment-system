import { combineReducers } from "redux";
import reducer from "./Shared/reducer";
import categoryReducer from "./Category/reducer";
import userReducer from "./Login/reducer";
import userRegisterReducer from "./Register/reducer";

export default combineReducers({
	reducer: reducer,
	categoryReducer: categoryReducer,
	userReducer: userReducer,
	registerReducer: userRegisterReducer
});
