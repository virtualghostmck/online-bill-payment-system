import { actionType as type } from "./type";

export const toggleRegisterDialogue = () => {
	return {
		type: type.TOGGLE_REGISTER_DIALOG
	};
};

export const toggleIsLoading = () => {
	return {
		type: type.TOGGLE_REGISTER_LOADING
	};
};

export const setSuccessReponse = data => {
	return {
		type: type.SET_SUCCESS_RESPONSE,
		payload: data
	};
};

export const setErrorResponse = data => {
	return {
		type: type.SET_ERROR_RESPONSE,
		payload: data
	};
};

export const clearSuccessResponse = () => {
	return {
		type: type.CLEAR_SUCCESS_RESPONSE
	};
};

export const clearErrorResponse = () => {
	return {
		type: type.CLEAR_ERROR_RESPONSE
	};
};
