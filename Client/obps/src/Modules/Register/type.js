import keyMirror from "keymirror";

export const actionType = keyMirror({
	TOGGLE_REGISTER_DIALOG: null,
	TOGGLE_REGISTER_LOADING: null,

	//API data
	SET_SUCCESS_RESPONSE: null,
	CLEAR_SUCCESS_RESPONSE: null,
	SET_ERROR_RESPONSE: null,
	CLEAR_ERROR_RESPONSE: null
});
