import { actionType as type } from "./type";

const initState = {
	openRegisterDialog: false,
	isRegisterLoading: false,
	successResponse: null,
	errorResponse: null
};

const userRegisterReducer = (state = initState, action) => {
	switch (action.type) {
		case type.TOGGLE_REGISTER_DIALOG:
			return {
				...state,
				openRegisterDialog: !state.openRegisterDialog
			};
		case type.TOGGLE_REGISTER_LOADING:
			return {
				...state,
				isRegisterLoading: !state.isRegisterLoading
			};
		case type.SET_SUCCESS_RESPONSE:
			return {
				...state,
				successResponse: action.payload
			};
		case type.SET_ERROR_RESPONSE:
			return {
				...state,
				errorResponse: action.payload
			};
		case type.CLEAR_SUCCESS_RESPONSE:
			return {
				...state,
				successResponse: null
			};
		case type.CLEAR_ERROR_RESPONSE:
			return {
				...state,
				errorResponse: null
			};
		default:
			return state;
	}
};

export default userRegisterReducer;
