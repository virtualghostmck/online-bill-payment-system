import { ActionTypes as types } from "./type";

export const setCategoryData = categoryData => {
  return {
    type: types.SET_CATEGORIES,
    payload: categoryData
  };
};

export const setCurrentCategory = currentCategory => {
  return {
    type: types.SET_CURRENT_CATEGORY,
    payload: currentCategory
  };
};
