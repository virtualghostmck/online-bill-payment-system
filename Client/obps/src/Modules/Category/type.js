import keyMirror from "keymirror";

export const ActionTypes = keyMirror({
  SET_CURRENT_CATEGORY: null,
  //API CALLS
  SET_CATEGORIES: null
});
