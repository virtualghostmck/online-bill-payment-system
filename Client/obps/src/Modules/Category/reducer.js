import { ActionTypes as actionType } from "./type";

var initialState = {
  categories: [...Array(5)],
  currentCategory: {
    categoryId: 1,
    categoryName: "Mobile",
    description: "Mobile Recharge"
  }
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actionType.SET_CATEGORIES:
      return {
        ...state,
        categories: action.payload
      };
    case actionType.SET_CURRENT_CATEGORY:
      return {
        ...state,
        currentCategory: action.payload
      };
    default:
      return state;
  }
}

export default reducer;
