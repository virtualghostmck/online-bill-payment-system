import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { withRouter } from "react-router";
import "./index.css";
import App from "./Components/App/App";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import store from "./Utils/store";
import "typeface-roboto";
import LoginContainer from "./Components/Login/LoginContainer";

const Root = props => {
	return (
		<Switch>
			<Route path="/" exact>
				<App />
			</Route>
		</Switch>
	);
};

const RootWithRouter = withRouter(Root);

ReactDOM.render(
	<Provider store={store}>
		<Router>
			<RootWithRouter />
		</Router>
	</Provider>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
