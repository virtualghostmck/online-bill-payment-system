import axios from "axios";
import { requestHandler } from "../Components/App/interceptor";

const axiosInstance = axios.create({
	baseURL: "https://172.27.31.105:9002/api"
});

// Add interceptors
axiosInstance.interceptors.request.use(request => requestHandler(request));

export default axiosInstance;
