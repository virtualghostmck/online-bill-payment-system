import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "../Modules/index";

var store = createStore(rootReducer, composeWithDevTools());

export default store;
