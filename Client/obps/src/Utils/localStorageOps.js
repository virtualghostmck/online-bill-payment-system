export const setUserToken = user => {
  localStorage.setItem("username", user.username);
  localStorage.setItem("token", user.token);
};

export const clearUserToken = () => {
  localStorage.clear();
};

export const getToken = () => {
  return localStorage.getItem("token");
};
