export const usernameValidator = username => {
	if (!username) return true;
	if (username.search(/^\w{3,8}$/) === 0) return true;
	else return false;
};

export const emailValidator = email => {
	if (email.search(/\S+@\S+\.\S+/) === 0) return true;
	else return false;
};

export const mobileNoValidator = mobile => {
	if (mobile.search(/^\d{10}$/) === 0) return true;
	else return false;
};
