import React from "react";
import { makeStyles, Grid, Paper } from "@material-ui/core";
import CategoryContainer from "./CategoryContainer";
import CategoryDetailsContainer from "./../CategoryDetails/CategoryDetails";

const useStyles = makeStyles(theme => ({
  root: {
    top: "59px",
    bottom: "45px",
    position: "absolute",
    backgroundColor: "#fff",
    padding: theme.spacing(1),
    margin: "auto",
    width: "100%",
    height: "auto"
  }
}));

const CategoryHome = props => {
  const styles = useStyles();
  return (
    <Grid className={styles.root} alignItems="stretch" spacing={2} container>
      {/* <Grid item xs={12} sm={3}>
        /* Category component here 
      </Grid> */}
      <CategoryContainer />
      <CategoryDetailsContainer />
    </Grid>
  );
};

export default CategoryHome;
