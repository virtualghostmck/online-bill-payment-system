import React, { Component } from "react";
import Categories from "./CategoryPresentation";
import { getCategories } from "../../API/Category_Service";
import { connect } from "react-redux";
import { toggleIsLoading } from "../../Modules/Shared/action";
import {
  setCategoryData,
  setCurrentCategory
} from "../../Modules/Category/action";
import Skeleton from "@material-ui/lab/Skeleton";
import { Grid } from "@material-ui/core";

class CategoryContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // categories: []
    };
  }

  componentDidMount() {
    getCategories()
      .then(response => {
        console.log(response);
        //use redux store to set the categories.
        // this.setState({ categories: response.data });
        this.props.setCategoryData(response.data);
        this.props.toggleIsLoading();
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleClick = categoryName => {
    const { categories } = this.props;
    const filterCategory = categories.filter(
      k => k.categoryName == categoryName
    );
    const [selectedCategory] = filterCategory;
    this.props.setCurrentCategory(selectedCategory);
  };

  render() {
    if (this.props.isLoading) {
      return (
        <Grid item xs={12} sm={3}>
          {[...Array(5)].map((value, index) => {
            return (
              <Skeleton
                key={index}
                animation="wave"
                variant="text"
                width={"100%"}
                height={"6%"}
              />
            );
          })}
          ;
        </Grid>
      );
    } else {
      return (
        <Categories
          handleClick={this.handleClick}
          categories={this.props.categories}
        />
      );
    }
  }
}

const mapStateToProps = state => ({
  isLoading: state.reducer.isLoading,
  categories: state.categoryReducer.categories
});

const mapDispatchToProps = {
  toggleIsLoading,
  setCategoryData,
  setCurrentCategory
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryContainer);
