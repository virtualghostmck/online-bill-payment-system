import React from "react";
import {
  makeStyles,
  Grid,
  Paper,
  IconButton,
  SvgIcon,
  Typography,
  Button
} from "@material-ui/core";
import PhoneAndroidTwoToneIcon from "@material-ui/icons/PhoneAndroidTwoTone";
import { lightBlue } from "@material-ui/core/colors";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    height: "auto",
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    alignItems: "center"
  },
  category: {
    width: "100%",
    padding: theme.spacing(1),
    flexBasis: "100%",
    flexGrow: 0,
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  buttonIcon: {
    width: "100%",
    justifyContent: "flex-start",
    backgroundColor: "aliceblue"
  },
  title: {
    fontWeight: 1000
  }
}));

const CategoryPresentation = props => {
  const { categories, handleClick } = props;
  const styles = useStyles();

  return (
    <Grid item xs={12} sm={3}>
      <Paper elevation={6} className={styles.paper}>
        <Typography className={styles.title} variant="h5">
          Category
        </Typography>
        {categories.map((category, index) => (
          <div key={index} className={styles.category}>
            <IconButton
              onClick={e => handleClick(category.categoryName)}
              style={{ border: "1px solid rgba(0,0,0,0.2)" }}
            >
              <SvgIcon>
                <PhoneAndroidTwoToneIcon />
              </SvgIcon>
            </IconButton>
            <Button
              className={styles.buttonIcon}
              variant="text"
              color="primary"
              onClick={e => handleClick(category.categoryName)}
            >
              <Typography style={{ textAlign: "left" }} variant="subtitle1">
                {` ${category.categoryName} `}
              </Typography>
            </Button>
          </div>
        ))}
      </Paper>
    </Grid>
  );
};

export default CategoryPresentation;
