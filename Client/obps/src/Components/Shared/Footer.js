import React, { Component } from "react";
import styles from "./../../Styles/Shared/footer.module.css";
import { AppBar, Toolbar, Typography } from "@material-ui/core";

export default class Footer extends Component {
  //   constructor(props) {
  //     super(props);

  //     this.state = {};
  //   }
  render() {
    return (
      <AppBar
        elevation={8}
        className={styles.footer}
        color="primary"
        position="absolute"
      >
        <Toolbar className={styles.toolbar}>
          <Typography variant="subtitle2">
            © 2019 Bill'O'Pay Inc. All rights reserved.
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}
