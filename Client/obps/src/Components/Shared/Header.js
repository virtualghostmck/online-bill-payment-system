import React, { Component } from "react";
import { connect } from "react-redux";
import {
	AppBar,
	Toolbar,
	Typography,
	Button,
	IconButton
} from "@material-ui/core";
import AccountCircleRoundedIcon from "@material-ui/icons/AccountCircleRounded";
import styles from "./../../Styles/Shared/header.module.css";
import LoginContainer from "./../Login/LoginContainer";
import RegisterContainer from "./../Register/RegisterContainer";
import { toggleLoginDialogue } from "../../Modules/Login/action";

class Header extends Component {
	constructor(props) {
		super(props);
	}

	handleClick = e => {
		this.props.toggleLoginDialogue();
	};

	render() {
		const { openLoginDialog, openRegisterDialog } = this.props;
		return (
			<AppBar elevation={8} color="primary" position="absolute">
				<Toolbar className={styles.toolbar}>
					<Typography className={styles.content} variant="h6">
						{this.props.siteName}
					</Typography>
					<Button
						variant="outlined"
						className={styles.button}
						color="inherit"
						onClick={this.handleClick}
					>
						<Typography variant="button">Login/Register</Typography>
					</Button>
					<IconButton>
						<AccountCircleRoundedIcon
							style={{ height: "50%" }}
							htmlColor="#fff"
							fontSize="large"
						/>
					</IconButton>
					{openLoginDialog && <LoginContainer />}
					{openRegisterDialog && <RegisterContainer />}
				</Toolbar>
			</AppBar>
		);
	}
}

var mapStateToProps = (state, props) => {
	return {
		siteName: state.reducer.siteName,
		openLoginDialog: state.userReducer.openLoginDialog,
		openRegisterDialog: state.registerReducer.openRegisterDialog
	};
};

const mapDispatchToProps = {
	toggleLoginDialogue
};
export default connect(mapStateToProps, mapDispatchToProps)(Header);
