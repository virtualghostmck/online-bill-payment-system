import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
	backGround: {
		backgroundColor: "rgba(0,0,0,0.5)",
		height: "100%",
		position: "fixed",
		width: "100%",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		top: "0"
	}
}));

const Spinner = props => {
	const { isLoading } = props;
	const classes = useStyles();

	if (isLoading) {
		return (
			<div className={classes.backGround}>
				<CircularProgress />
			</div>
		);
	} else {
		return null;
	}
};
export default Spinner;
