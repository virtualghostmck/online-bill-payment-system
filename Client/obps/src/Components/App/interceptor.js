import { getToken } from "../../Utils/localStorageOps";

const isHandlerEnabled = (config = {}) => {
  return config.hasOwnProperty("handlerEnabled") && !config.handlerEnabled
    ? false
    : true;
};

export const requestHandler = request => {
  if (isHandlerEnabled(request)) {
    request.headers["Authorization"] = `Bearer ${getToken()}`;
  }
  return request;
};
