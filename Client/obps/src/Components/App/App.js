import React from "react";
import Header from "../../Components/Shared/Header";
import Container from "@material-ui/core/Container";
import Footer from "../../Components/Shared/Footer";
import Home from "./../../Components/Categories/CategoryHome";
import styles from "./../../Styles/App/app.module.css";
import CssBaseline from "@material-ui/core/CssBaseline";

function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Container className={styles.root} disableGutters maxWidth="lg">
        <Header />
        <Home />
        <Footer />
      </Container>
    </React.Fragment>
  );
}

export default App;
