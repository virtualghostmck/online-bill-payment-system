import React from "react";
import {
  TextField,
  makeStyles,
  RadioGroup,
  FormControlLabel,
  Radio,
  Grid
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  input: {
    height: "10%",
    width: "35%",
    marginBottom: "4%"
  },
  radioGroup: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "3%"
  }
}));

const CategoryDetailsPresentation = props => {
  const { handleChange, selectedCategory } = props;
  const classes = useStyles();
  return (
    <React.Fragment>
      <Grid item container alignItems="center">
        <TextField
          size="medium"
          variant="outlined"
          placeholder={
            selectedCategory ? selectedCategory.categoryName : "Mobile"
          }
          name={selectedCategory.categoryName}
          label={
            selectedCategory
              ? selectedCategory.description
              : "Enter Mobile Number"
          }
          margin="dense"
          helperText="Provide the details here"
          color="primary"
          autoFocus={true}
          className={classes.input}
        />
        {selectedCategory.categoryName === "Mobile" && (
          <RadioGroup
            aria-label="type"
            name="type"
            // value={value}
            onChange={handleChange}
            className={classes.radioGroup}
          >
            <FormControlLabel
              value="Prepaid"
              control={<Radio />}
              label="Prepaid"
            />
            <FormControlLabel
              value="Postpaid"
              control={<Radio />}
              label="PostPaid"
            />
          </RadioGroup>
        )}
      </Grid>
    </React.Fragment>
  );
};

export default CategoryDetailsPresentation;
