import React, { Component } from "react";
import { Paper, Grid, withStyles, Typography, Button } from "@material-ui/core";
import ForwardRoundedIcon from "@material-ui/icons/ForwardRounded";
import CategoryDetailsPresentation from "./CategoryDetailsPresentation";
import { connect } from "react-redux";
import Skeleton from "@material-ui/lab/Skeleton";
import { toggleLoginDialogue } from "../../Modules/Login/action";
const styles = theme => ({
	paper: {
		width: "100%",
		height: "35%",
		padding: theme.spacing(2)
	},
	title: {
		fontWeight: "1000"
	},
	innerGrid: {
		flexDirection: "column",
		height: "100%"
	},
	button: {
		width: "18%",
		justifyContent: "space-evenly"
	}
});

class CategoryDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	handleClick = event => {
		const { currentUser } = this.props;
		if (!currentUser) {
			this.props.toggleLoginDialogue();
			return;
		}
	};

	render() {
		const { classes, isLoading, currentCategory } = this.props;
		if (isLoading) {
			return (
				<Grid item xs={12} sm={9}>
					{[...Array(5)].map((value, index) => {
						return (
							<Skeleton
								key={index}
								animation="wave"
								variant="text"
								width={"100%"}
								height={"6%"}
							/>
						);
					})}
				</Grid>
			);
		} else {
			return (
				<React.Fragment>
					<Grid item xs={12} sm={9}>
						<Paper elevation={6} className={classes.paper}>
							<Grid
								className={classes.innerGrid}
								item
								container
								justify="space-between"
								alignItems="flex-start"
							>
								<Typography className={classes.title} variant="h5">
									{currentCategory.description}
								</Typography>
								<CategoryDetailsPresentation
									selectedCategory={currentCategory}
								/>
								<Button
									className={classes.button}
									color="primary"
									variant="contained"
									size="large"
									endIcon={<ForwardRoundedIcon color="inherit" />}
									onClick={this.handleClick}
								>
									<Typography variant="button">PROCEED</Typography>
								</Button>
							</Grid>
						</Paper>
					</Grid>
				</React.Fragment>
			);
		}
	}
}
const mapStateToProps = state => {
	return {
		isLoading: state.reducer.isLoading,
		categories: state.categoryReducer.categories,
		currentCategory: state.categoryReducer.currentCategory,
		currentUser: state.userReducer.currentUser
	};
};

const mapDispatchToProps = {
	toggleLoginDialogue
};

const CategoryDetailsWithStyles = withStyles(styles)(CategoryDetails);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CategoryDetailsWithStyles);
