import React, { Component } from "react";
import { connect } from "react-redux";
import RegisterForm from "./RegisterForm";
import {
	toggleRegisterDialogue,
	toggleIsLoading,
	setErrorResponse,
	setSuccessReponse,
	clearErrorResponse,
	clearSuccessResponse
} from "../../Modules/Register/action";
import { toggleLoginDialogue } from "../../Modules/Login/action";
import {
	usernameValidator,
	mobileNoValidator,
	emailValidator
} from "../../Utils/validator";
import Snackbar from "@material-ui/core/Snackbar";
import { registerUser } from "../../API/Register_Service";

class RegisterContainer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: "",
			contactNo: "",
			email: "",
			password: "",
			usernameError: false,
			emailError: false,
			mobileError: false
		};
	}

	openLoginForm = e => {
		e.preventDefault();
		this.props.toggleRegisterDialogue();
		this.props.toggleLoginDialogue();
	};

	handleSubmit = e => {
		//make api call

		console.log(this.state);
		const payLoad = {
			UserName: this.state.username,
			Password: this.state.password,
			ContactNo: this.state.contactNo,
			Email: this.state.email
		};
		this.props.toggleIsLoading();
		registerUser(payLoad)
			.then(response => {
				console.log(response);
				this.props.toggleIsLoading();
			})
			.catch(error => {
				console.log(error);
				this.props.toggleIsLoading();
			});
	};

	handleChange = e => {
		const { id, value } = e.target;
		this.setState(prevState => ({
			[id]: value
		}));
		switch (id) {
			case "username":
				this.setState({
					usernameError: !usernameValidator(value)
				});
				break;
			case "contactNo":
				this.setState({
					mobileError: !mobileNoValidator(value)
				});
				break;
			case "email":
				this.setState({
					emailError: !emailValidator(value)
				});
				break;
			default:
				break;
		}
	};

	handleClose = e => {
		this.props.toggleRegisterDialogue();
	};

	render() {
		const { openRegisterDialog } = this.props;
		return (
			<React.Fragment>
				<RegisterForm
					isLoading={this.props.isRegisterLoading}
					isOpen={openRegisterDialog}
					handleClose={this.handleClose}
					formData={this.state}
					handleChange={this.handleChange}
					handleSubmit={this.handleSubmit}
					openLoginForm={this.openLoginForm}
				/>
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => ({
	openRegisterDialog: state.registerReducer.openRegisterDialog,
	isRegisterLoading: state.registerReducer.isRegisterLoading,
	successResponse: state.registerReducer.successResponse,
	errorResponse: state.registerReducer.errorResponse
});

const mapDispatchToProps = {
	toggleRegisterDialogue,
	toggleLoginDialogue,
	toggleIsLoading
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer);
