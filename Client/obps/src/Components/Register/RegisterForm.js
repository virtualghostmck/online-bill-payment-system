import React from "react";
import Link from "@material-ui/core/Link";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Icon, Grid, Paper } from "@material-ui/core";
import Spinner from "../Shared/Spinner";

const useStyles = makeStyles(theme => ({
	title: {
		textAlign: "center"
	},
	icon: {
		height: theme.spacing(5),
		width: theme.spacing(5),
		alignSelf: "center",
		marginTop: theme.spacing(2)
	},
	grid: {
		flexDirection: "column"
	},
	paper: {
		borderRadius: "4%",
		flexDirection: "column",
		display: "flex"
	},
	input: {
		marginTop: theme.spacing(3)
	}
}));

export default function RegisterForm(props) {
	const {
		isOpen,
		handleClose,
		formData,
		handleChange,
		handleSubmit,
		openLoginForm,
		isLoading
	} = props;
	const classes = useStyles();
	const {
		email,
		username,
		contactNo,
		password,
		usernameError,
		emailError,
		mobileError
	} = formData;

	return (
		<div>
			<Dialog
				open={isOpen}
				onClose={handleClose}
				aria-labelledby="form-dialog-title"
				disableBackdropClick
				PaperProps={{
					classes: { root: classes.paper }
				}}
			>
				<Icon className={classes.icon}>
					<img style={{ height: "100%" }} src="/Icons/login.svg" />
				</Icon>
				<DialogContent>
					<Grid
						container
						justify="space-evenly"
						alignItems="center"
						className={classes.grid}
					>
						<TextField
							autoFocus
							error={usernameError}
							margin="dense"
							id="username"
							label="Username"
							type="text"
							autoComplete="new-password"
							variant="outlined"
							onChange={handleChange}
							size="small"
							required
							className={classes.input}
						/>
						<TextField
							error={emailError}
							margin="dense"
							id="email"
							label="Email Address"
							type="email"
							autoComplete="new-password"
							variant="outlined"
							onChange={handleChange}
							size="small"
							required
							className={classes.input}
						/>
						<TextField
							error={mobileError}
							margin="dense"
							id="contactNo"
							label="Mobile"
							type="number"
							autoComplete="new-password"
							variant="outlined"
							onChange={handleChange}
							size="small"
							required
							className={classes.input}
						/>
						<TextField
							className={classes.input}
							margin="dense"
							id="password"
							variant="outlined"
							label="Password"
							type="password"
							autoComplete="new-password"
							onChange={handleChange}
							required
							size="small"
						/>
						<Typography style={{ paddingTop: "7px" }} variant="caption">
							Already an user?{" "}
							<Link onClick={openLoginForm} href="#">
								Login
							</Link>
						</Typography>
					</Grid>
				</DialogContent>
				<DialogActions style={{ paddingTop: "0px", justifyContent: "center" }}>
					<Spinner isLoading={isLoading} />
					<Button
						onClick={handleSubmit}
						color="primary"
						disabled={
							!email ||
							!password ||
							usernameError ||
							mobileError ||
							emailError ||
							isLoading
						}
					>
						REGISTER
					</Button>
					<Button onClick={handleClose} disabled={isLoading} color="primary">
						CANCEL
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}
