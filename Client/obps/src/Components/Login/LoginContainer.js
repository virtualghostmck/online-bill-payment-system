import React, { Component } from "react";
import { connect } from "react-redux";
import LoginForm from "./LoginForm";
import { setUserData, toggleLoginDialogue } from "../../Modules/Login/action";
import { toggleRegisterDialogue } from "../../Modules/Register/action";

class LoginContainer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: "",
			password: ""
		};
	}

	openRegisterForm = e => {
		console.log("open register");
		e.preventDefault();
		this.props.toggleLoginDialogue();
		this.props.toggleRegisterDialogue();
	};

	handleSubmit = () => {
		console.log(this.state);
		//make api call
	};

	handleChange = e => {
		const { id, value } = e.target;
		console.log(id, value);
		this.setState({
			[id]: value
		});
	};

	handleClose = e => {
		this.props.toggleLoginDialogue();
	};

	render() {
		console.log("rendered container");
		const { openLoginDialog } = this.props;
		return (
			<LoginForm
				isOpen={openLoginDialog}
				handleClose={this.handleClose}
				formData={this.state}
				handleChange={this.handleChange}
				handleSubmit={this.handleSubmit}
				openRegisterForm={this.openRegisterForm}
			/>
		);
	}
}

const mapStateToProps = state => ({
	openLoginDialog: state.userReducer.openLoginDialog
});

const mapDispatchToProps = {
	setUserData,
	toggleLoginDialogue,
	toggleRegisterDialogue
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
