export const category = {
	GET_CATEGORIES: "/Category"
};

export const vendors = {
	GET_VENDORS: "/vendor"
};

export const AUTH = {
	LOGIN_USER: "",
	REGISTER_USER: "/Account/Register"
};
