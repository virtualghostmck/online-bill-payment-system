import axiosInstance from "../Utils/axiosInstance";
import { category, vendors } from "../API/Endpoint";

export function getCategories() {
	return axiosInstance.get(category.GET_CATEGORIES, false);
}

export function getVendorsInCategory(categoryName) {
	return axiosInstance.get(`${vendors.GET_VENDORS}/${categoryName}`, false);
}
