import axiosInstance from "../Utils/axiosInstance";
import { AUTH } from "../API/Endpoint";

export const registerUser = payLoad => {
	return axiosInstance.post(AUTH.REGISTER_USER, payLoad, false);
};
